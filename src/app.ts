import * as express from 'express';
import * as bp from 'body-parser';
// import * as cors from 'cors';

const app = express();
app.use(bp.json());
// app.use(cors());

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log(`Request ${req.method} ${req.originalUrl} at ${new Date()}`);
    next();
});

import { itemRoutes } from './routes/items';
app.use('/items', itemRoutes);

import { userRoutes } from './routes/users';
app.use('/users', userRoutes);

import { storeRoutes } from './routes/stores';
app.use('/stores', storeRoutes);

import { orderRoutes } from './routes/orders';
app.use('/orders', orderRoutes);

export { app };
