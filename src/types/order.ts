import { Document } from "mongoose"
import { IItem } from "./item"
import { IUser } from "./user"


export interface IOrder extends Document {
  quantity: number
  item: IItem
  status: 'placed' | 'shipped'
}