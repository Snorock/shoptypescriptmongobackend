import { Document } from "mongoose"
import { IItem } from "./item"
import { IUser } from "./user"


export interface IStore extends Document {
  name: string
  description: string
  items: IItem[]
}