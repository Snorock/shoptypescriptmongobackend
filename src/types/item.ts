import { Document } from "mongoose"
import { IStore } from "./store"

export interface IItem extends Document {
  name: string
  description: string
  unit_price: number
  quantity: number
}