import { Document } from "mongoose"
import { IStore } from "./store"
import { IOrder } from "./order"

export interface IUser extends Document {
  name: string
  email: string
  password: string
  address: string
  balance: number
  stores: IStore[]
  orders: IOrder[]
}