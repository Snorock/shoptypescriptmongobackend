import { Response, Request } from "express";
import { IStore } from "./../types/store";
import { IUser } from "./../types/user";
import Store from "./../models/store";
import User from "./../models/user";

const getStores = async (req: Request, res: Response): Promise<void> => {
    try {
        let params: object = {};
        if(req.params.id){
            params = { _id: req.params.id };
        }
        const stores: IStore[] = await Store.find(params);
        res.status(200).json({ stores });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const addStore = async (req: Request, res: Response): Promise<void> => {
    try {
        let user: IUser = await User.findOne({_id:req.body.ownerId});
        const store: IStore = new Store({
            name: req.body.name,
            description: req.body.description || "",
            items: [],
        });
        const newStore: IStore = await store.save();
        user.stores.push(newStore);
        const updatedUser: IUser | null = await User.findByIdAndUpdate(
            { _id: req.body.ownerId },
            user
        );
        res.status(201).json({ newStore, updatedUser });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const updateStore = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatedStore: IStore | null = await Store.findByIdAndUpdate(
            { _id: req.params.id },
            req.body
        );

        res.status(200).json({ updatedStore });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const deleteStore = async (req: Request, res: Response): Promise<void> => {
    try {
        const deletedStore: IStore | null = await Store.findByIdAndRemove({ _id: req.params.id });
        let user: IUser = await User.findOne({stores:deletedStore._id});
        let ind = user.stores.indexOf(deletedStore._id);
        if(ind>-1){
            user.stores.splice(ind, 1);
            await User.findByIdAndUpdate(
                { _id: user._id },
                user
            );
        }

        res.status(200).json({ deletedStore });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}
      
export {getStores, addStore, updateStore, deleteStore};