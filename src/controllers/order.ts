import { Response, Request } from "express";
import { IItem } from "./../types/item";
import { IUser } from "./../types/user";
import { IOrder } from "./../types/order";
import Item from "./../models/item";
import User from "./../models/user";
import Order from "./../models/order";

const getOrders = async (req: Request, res: Response): Promise<void> => {
    try {
        let params: object = {};
        if(req.params.id){
            params = { _id: req.params.id };
        }
        const orders: IOrder[] = await Order.find(params);
        res.status(200).json({ orders });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const addOrder = async (req: Request, res: Response): Promise<void> => {
    try {
        let item: IItem = await Item.findOne({_id:req.body.itemid});
        let user: IUser = await User.findOne({_id:req.body.userid});
        const order: IOrder = new Order({
            quantity: req.body.quantity,
            item: item,
            status: 'placed'
        });
        item.quantity = item.quantity - req.body.quantity;
        if(item.quantity<0){
            throw new Error("Insaficient stock");
        }
        const newOrder: IOrder = await order.save();
        user.orders.push(newOrder);
        user.balance = user.balance - (req.body.quantity*item.unit_price);
        const updatedUser: IUser | null = await User.findByIdAndUpdate(
            { _id: req.body.userid},
            user
        );
        const updatedItem: IItem| null = await Item.findByIdAndUpdate(
            { _id: req.body.itemid },
            { quantity: item.quantity }
        );
        res.status(201).json({ newOrder, updatedUser, updatedItem });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const updateOrder = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatedOrder: IOrder| null = await Order.findByIdAndUpdate(
            { _id: req.params.id },
            req.body
        );

        res.status(200).json({ updatedOrder });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const deleteOrder = async (req: Request, res: Response): Promise<void> => {
    try {
        const deletedOrder: IOrder | null = await Order.findByIdAndRemove({ _id: req.params.id });
        let user: IUser = await User.findOne({orders:deletedOrder._id});
        let ind = user.orders.indexOf(deletedOrder._id);
        if(ind>-1){
            user.orders.splice(ind, 1);
            await User.findByIdAndUpdate(
                { _id: user._id },
                user
            );
        }

        res.status(200).json({ deletedOrder });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const shipOrders = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatedOrder: IOrder| null = await Order.updateMany(
            { status: 'placed' },
            { status: 'shipped' },
        );

        res.status(200).json({ updatedOrder });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

export {getOrders, addOrder, updateOrder, deleteOrder, shipOrders};