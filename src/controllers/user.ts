import { Response, Request, NextFunction } from "express";
import { IUser } from "./../types/user";
import User from "./../models/user";

const bcrypt = require("bcrypt");
const saltRounds = 10;

const getUsers = async (req: Request, res: Response): Promise<void> => {
    try {
        let params: object = {};
        if(req.params.id){
            params = { _id: req.params.id };
        }
        const users: IUser[] = await User.find(params);
        res.status(200).json({ users });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const loginUser = async (req: Request, res: Response): Promise<void> => {
    try {
        const params = {
            email: req.body.login
        };
        const user: IUser = await User.findOne(params);
        const correctPass: boolean = await bcrypt.compare(req.body.password, user.password);
        if(!correctPass){
            throw "Wrong Password";
        }
        res.status(200).json({ user });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const checkIsLogin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        const params = {
            _id: req.headers['token']
        };
        const user: IUser = await User.findOne(params);
        if(!user){
            throw "Action requires login";
        }
        next()
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const addUser = async (req: Request, res: Response): Promise<void> => {
    try {
        const hash = await bcrypt.hash(req.body.password, saltRounds);
        const user: IUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: hash,
            address: req.body.address || "",
            balance: req.body.unit_price || 0,
            stores: [],
            orders: [],
        });
        const newUser: IUser = await user.save();
        res.status(201).json({ newUser });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const updateUser = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatedUser: IUser | null = await User.findByIdAndUpdate(
            { _id: req.params.id },
            req.body
        );

        res.status(200).json({ updatedUser });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const deleteUser = async (req: Request, res: Response): Promise<void> => {
    try {
        const deletedUser: IUser | null = await User.findByIdAndRemove({ _id: req.params.id });

        res.status(200).json({ deletedUser });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}
      
export {getUsers, loginUser, checkIsLogin, addUser, updateUser, deleteUser};