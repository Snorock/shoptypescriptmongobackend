import { Response, Request } from "express";
import { IItem } from "./../types/item";
import { IStore } from "./../types/store";
import Item from "./../models/item";
import Store from "./../models/store";

const getItems = async (req: Request, res: Response): Promise<void> => {
    try {
        let params: object = {};
        if(req.params.id){
            params = { _id: req.params.id };
        }
        const items: IItem[] = await Item.find(params);
        res.status(200).json({ items });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error });
    }
}

const addItem = async (req: Request, res: Response): Promise<void> => {
    try {
        let store: IStore = await Store.findOne({_id:req.body.storeid});
        const item: IItem = new Item({
            name: req.body.name,
            description: req.body.description || "",
            unit_price: req.body.unit_price || 0,
            quantity: req.body.quantity || 0,
        });
        const newItem: IItem = await item.save();
        store.items.push(newItem);
        const updatedStore: IStore | null = await Store.findByIdAndUpdate(
            { _id: req.body.storeid},
            store
        );
        res.status(201).json({ newItem, updatedStore });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const updateItem = async (req: Request, res: Response): Promise<void> => {
    try {
        const updatedItem: IItem| null = await Item.findByIdAndUpdate(
            { _id: req.params.id },
            req.body
        );

        res.status(200).json({ updatedItem });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

const deleteItem = async (req: Request, res: Response): Promise<void> => {
    try {
        const deletedItem: IItem | null = await Item.findByIdAndRemove({ _id: req.params.id });
        let store: IStore = await Store.findOne({items:deletedItem._id});
        let ind = store.items.indexOf(deletedItem._id);
        if(ind>-1){
            store.items.splice(ind, 1);
            await Store.findByIdAndUpdate(
                { _id: store._id },
                store
            );
        }

        res.status(200).json({ deletedItem });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

export {getItems, addItem, updateItem, deleteItem};