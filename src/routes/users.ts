import * as express from 'express';
import { getUsers, loginUser, addUser, updateUser, deleteUser, checkIsLogin} from './../controllers/user';

const userRoutes = express.Router();

userRoutes.get('/:id?', getUsers);

userRoutes.post('/login', loginUser);

userRoutes.post('/add', addUser);

userRoutes.post('/update/:id', checkIsLogin, updateUser);

userRoutes.post('/delete/:id', checkIsLogin, deleteUser);

export { userRoutes };