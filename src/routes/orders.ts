import * as express from 'express';
import { getOrders, addOrder, updateOrder, deleteOrder, shipOrders } from './../controllers/order';
import { checkIsLogin } from './../controllers/user';

const orderRoutes = express.Router();

orderRoutes.get('/:id?', getOrders);

orderRoutes.post('/add', checkIsLogin, addOrder);

orderRoutes.post('/update/:id', checkIsLogin, updateOrder);

orderRoutes.post('/delete/:id', checkIsLogin, deleteOrder);

orderRoutes.post('/ship', shipOrders);

export { orderRoutes };