import * as express from 'express';
import { getStores, addStore, updateStore, deleteStore } from './../controllers/store';
import { checkIsLogin } from './../controllers/user';

const storeRoutes = express.Router();

storeRoutes.get('/:id?', getStores);

storeRoutes.post('/add', checkIsLogin, addStore);

storeRoutes.post('/update/:id', checkIsLogin, updateStore);

storeRoutes.post('/delete/:id', checkIsLogin, deleteStore);

export { storeRoutes };