import * as express from 'express';
import { getItems, addItem, updateItem, deleteItem } from './../controllers/item';
import { checkIsLogin } from './../controllers/user';

const itemRoutes = express.Router();

itemRoutes.get('/:id?', getItems);

itemRoutes.post('/add', checkIsLogin, addItem);

itemRoutes.post('/update/:id', checkIsLogin, updateItem);

itemRoutes.post('/delete/:id', checkIsLogin, deleteItem);

export { itemRoutes };