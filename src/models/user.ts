import { IUser } from "./../types/user"
import { model, Schema } from "mongoose"

const userSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },

    email: {
      type: String,
      required: true,
      unique: true
    },

    password: {
      type: String,
      required: true,
    },

    address: String,

    balance: Number,

    stores: [{ type:  Schema.Types.ObjectId, ref: 'Store' }],

    orders: [{ type:  Schema.Types.ObjectId, ref: 'Order' }],
  }
)

export default model<IUser>("User", userSchema)