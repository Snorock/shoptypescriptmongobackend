import { IItem } from "./../types/item"
import { model, Schema } from "mongoose"

const itemSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },

    description: {
      type: String,
    },

    unit_price: Number,

    quantity: Number,

  }
)

export default model<IItem>("Item", itemSchema)