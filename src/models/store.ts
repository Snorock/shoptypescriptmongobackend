import { IStore } from "./../types/store"
import { model, Schema } from "mongoose"

const storeSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },

    description: {
      type: String,
    },

    items: [{ type:  Schema.Types.ObjectId, ref: 'Item' }],
  }
)

export default model<IStore>("Store", storeSchema)