import { IOrder } from "./../types/order"
import { model, Schema } from "mongoose"

const orderSchema: Schema = new Schema(
  {
    quantity: {
      type: Number,
      required: true,
    },

    item: { type:  Schema.Types.ObjectId, ref: 'Item' },

    status: String
  }
)

export default model<IOrder>("Order", orderSchema)