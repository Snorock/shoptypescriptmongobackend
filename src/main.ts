import * as http from 'http';
import * as mongoose from 'mongoose';
import {app} from './app';

const PORT = 8081;
const URI = "mongodb+srv://admin:admin@cluster0.3peh3.mongodb.net/shopDB?retryWrites=true&w=majority";
const server = http.createServer(app);
server.listen(PORT);
server.on('listening', () => {
    console.log('Server running on port ' + PORT);
    mongoose.connect(URI, {useNewUrlParser:true});
    mongoose.connection.on('open', () => {
        console.log('Connected to Mongo');
    });
    mongoose.connection.on('error', (err: any) => {
        console.error(err);
    });
});